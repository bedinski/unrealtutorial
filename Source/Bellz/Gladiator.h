// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "GameDataTables.h"
#include "GameFramework/Character.h"
#include "Gladiator.generated.h"

UCLASS(config = Game)
class BELLZ_API AGladiator : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AGladiator();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//Camera boom positioning the camera behind the character
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAcess = "true"))
	class USpringArmComponent* CameraBoom;

	//Follow camera
	UPROPERTY(VisibleAnywhere, BlueprintReadyOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

	//The sprite used to draw effect, better and more contrallable than using the HUD or Textures
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Effects, meta = (AllowPrivateAccess = "true"))
	class UPaperSpriteComponent* EffectSprite;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	
	
};
